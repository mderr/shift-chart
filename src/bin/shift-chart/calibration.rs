use std::io::ErrorKind;

use crate::chart::Chart;
use crate::config::Config;
use crate::bar::ProgressBar;

fn range_in_steps( start: f32, end: f32, number_of_steps: u64 ) -> Vec<f32> {
    let mut steps_vec: Vec<f32> = Vec::new();
    let step_size = (end - start) / number_of_steps as f32;
    
    for number in 0..number_of_steps as i64 {
        steps_vec.push( start + ( step_size * number as f32) );
    }

    steps_vec
}

pub trait Calibrator {
    fn calibrate_scores( &mut self, progress_bar: &mut ProgressBar );
}

impl Calibrator for Config {
    fn calibrate_scores( &mut self, progress_bar: &mut ProgressBar ) {
        let mut calibration_config = self.clone();
        let mut best_balance_deviation = 100.0;
        let mut best_scores = ( 0.0, 0.0, 0.0 );
        let mut calibration_rounds = 10;
        
        let mut calibration_calculations = (self.calculations as f64 * 0.0005).round() as u64;
        
        if calibration_calculations == 0 {
            calibration_calculations = 1;
            calibration_rounds = 5;
        }

        let total_calculations = calibration_calculations * calibration_rounds * calibration_rounds * calibration_rounds;
    
        let prev_action_message = progress_bar.action_message.clone();
        let prev_job_char = progress_bar.job_char.clone();

        progress_bar.action_message = "Calibrating scores...".to_string();
        progress_bar.job_char = 'C';
        progress_bar.number_of_jobs += total_calculations;

        let mut solved_calibration_charts: Vec<Chart> = Vec::new();
    
        calibration_config.calculations = calibration_calculations;
    
        for availability_bias in range_in_steps(0.01, 0.11, calibration_rounds) {
            calibration_config.availability_score_bias = availability_bias;
    
            for contract_bias in range_in_steps(0.01, 0.11, calibration_rounds) {    
                calibration_config.contract_score_bias = contract_bias;
    
                for worked_bias in range_in_steps(0.2, 0.8, calibration_rounds) {
                    calibration_config.worked_score_bias = worked_bias;
                   
                    let mut calibration_chart = Chart::from_config(&calibration_config);
                    
                    match calibration_chart.solve_for_calibration(&mut solved_calibration_charts, &calibration_config, progress_bar) {
                        Ok(_) => (),
                        Err(error) => match error.kind() {
                            ErrorKind::Deadlock => (),
                            _ => {
                                solved_calibration_charts.sort_by( 
                                    |a, b| a.get_balance_deviation().partial_cmp(&b.get_balance_deviation()).unwrap()
                                );

                                let round_best_balance = solved_calibration_charts[0].get_balance_deviation();
    
                                if  round_best_balance< best_balance_deviation {
                                    best_balance_deviation = round_best_balance;
                                    best_scores = (availability_bias, contract_bias, worked_bias);
                                }
                
                                solved_calibration_charts.clear()
                            }
                        }
                    }
                }
            }
        }

        progress_bar.action_message = prev_action_message;
        progress_bar.job_char = prev_job_char;
    
        self.availability_score_bias = best_scores.0;
        self.contract_score_bias = best_scores.1;
        self.worked_score_bias = best_scores.2;
    }
}
