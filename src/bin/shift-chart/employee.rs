use std::collections::HashMap;

use crate::shift::{Shift, ShiftKind};

#[derive(Debug, Clone)]
pub struct Employee {
    pub index: usize,
    pub name: String,
    pub shift_counts: HashMap<ShiftKind, u8>,
    pub contract_hours: u16,
    pub worked_hours: u16,

    // Flags
    pub is_assigned: bool,
    pub is_resting: bool,
    pub is_overworked: bool,
    pub evening_blocked: bool,
    pub is_best_afternoon: bool,

    // Scores
    pub availability_score: f32,
    pub contract_score: f32,
    pub worked_score: f32,
    pub afternoon_score: f32,
    pub fasttrack_score: f32,

    // Assigned
    pub assigned_evenings: u16,
    pub assigned_mornings: u16,
    pub assigned_afternoons: u16,
    pub assigned_fasttracks: u16,
}

impl Employee {
    pub fn new( index: usize, name: String, contract_hours: u16 ) -> Employee {
        let mut shift_counts: HashMap<ShiftKind, u8> = HashMap::new();

        for shift in ShiftKind::iterator() {
            shift_counts.insert(*shift, 0);
        };

        Employee {
            index,
            name,
            shift_counts,
            contract_hours,
            worked_hours: 0,
            is_assigned: false,
            is_resting: false,
            is_overworked: false,
            evening_blocked: false,
            is_best_afternoon: false,
            availability_score: 0.0,
            contract_score: 0.0,
            worked_score: 0.0,
            afternoon_score: 0.0,
            fasttrack_score: 0.0,
            assigned_evenings: 0,
            assigned_mornings: 0,
            assigned_afternoons: 0,
            assigned_fasttracks: 0,
        }
    }

    pub fn reset_flags( &mut self ) {
        self.is_assigned = false;
        self.is_resting = false;
        self.is_overworked = false;
        self.evening_blocked = false;
        self.is_best_afternoon = false;
    }

    

    pub fn add_shift( &mut self, shift_kind: ShiftKind ) {
        let count = self.shift_counts.entry(shift_kind).or_insert(0);

        match shift_kind {
            ShiftKind::Evening => self.assigned_evenings += 1,
            ShiftKind::Morning => self.assigned_mornings += 1,
            ShiftKind::FastTrack => self.assigned_fasttracks += 1,
            ShiftKind::Afternoon => self.assigned_afternoons += 1,
            _ => (),
        }
        
        *count += 1;
        self.worked_hours += shift_kind.duration() as u16;
    }

    pub fn remove_shift( &mut self, shift_kind: ShiftKind ) {
        let count = self.shift_counts.entry(shift_kind).or_insert(0);

        match shift_kind {
            ShiftKind::Evening => self.assigned_evenings -= 1,
            ShiftKind::Morning => self.assigned_mornings -= 1,
            ShiftKind::FastTrack => self.assigned_fasttracks -= 1,
            ShiftKind::Afternoon => self.assigned_afternoons -= 1,
            _ => (),
        }
        
        *count -= 1;
        self.worked_hours -= shift_kind.duration() as u16;
    }

    pub fn get_general_score( &self ) -> f32 {
        self.availability_score + self.contract_score + self.worked_score
    }

    pub fn is_unassignable( &self ) -> bool {
        if self.is_overworked || self.is_assigned {
            return true
        }

        if self.is_resting && self.evening_blocked {
            return true
        }

        false
    }

    pub fn count_assigned_shifts( &mut self, assigned_shifts: Vec<Shift> ) {
        for shift in assigned_shifts {    
            self.add_shift(shift.kind);
        }
    }

    pub fn get_shift_kind_order( &mut self ) -> Vec<ShiftKind> {
        let mut shift_kind_order: Vec<ShiftKind> = Vec::new();
    
        if self.is_resting && !self.evening_blocked {
            shift_kind_order.push(ShiftKind::Evening);
        }
    
        if self.is_best_afternoon && !self.is_resting {
            shift_kind_order.push(ShiftKind::Afternoon);
    
            if self.assigned_evenings >= self.assigned_mornings {
                shift_kind_order.push(ShiftKind::Morning);
    
                if !self.evening_blocked {
                    shift_kind_order.push(ShiftKind::Evening);
                }
                    
            } else {
                if !self.evening_blocked {
                    shift_kind_order.push(ShiftKind::Evening);
                    shift_kind_order.push(ShiftKind::Morning);
                }
            }
    
        } else {
            if !self.is_resting {
                if self.assigned_evenings >= self.assigned_mornings {
                    shift_kind_order.push(ShiftKind::Morning);
    
                    if !self.evening_blocked {
                        shift_kind_order.push(ShiftKind::Evening)
                    }
    
                } else {
                    if !self.evening_blocked {
                        shift_kind_order.push(ShiftKind::Evening);
                        shift_kind_order.push(ShiftKind::Morning);
                    }
                }
    
                shift_kind_order.push(ShiftKind::Afternoon);
            }            
        }
    
        shift_kind_order
    }

    pub fn get_balance( &self ) -> i16 {
        self.worked_hours as i16 - self.contract_hours as i16
    }
}