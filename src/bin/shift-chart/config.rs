#[derive(Clone)]
pub struct Config {
    pub calculations: u64,
    pub solutions: u64,
    pub workbook_filepath: String,
    pub worksheet_name: Option<String>,
    pub be_quiet: bool,
    pub run_api: bool,

    // Scores
    pub availability_score_bias: f32,
    pub contract_score_bias: f32,
    pub worked_score_bias: f32,
    pub afternoon_balance_bias: f32,
    pub afternoon_score_bias: f32,
}

impl Config {
    pub fn new() -> Self {
        Self {
            calculations: 10,
            solutions: 1,
            workbook_filepath: "".to_string(),
            worksheet_name: None,
            be_quiet: false,
            run_api: false,
            availability_score_bias: 0.09,
            contract_score_bias: 0.05,
            worked_score_bias: 0.50,
            afternoon_balance_bias: 0.05,
            afternoon_score_bias: 0.10,
        }
    }

    pub fn from( calculations: u64, solutions: u64, workbook_filepath: String, worksheet_name: Option<String> ) -> Self {
        Self {
            calculations,
            solutions,
            workbook_filepath,
            worksheet_name,
            be_quiet: false,
            run_api: false,
            availability_score_bias: 0.09,
            contract_score_bias: 0.05,
            worked_score_bias: 0.50,
            afternoon_balance_bias: 0.05,
            afternoon_score_bias: 0.10,
        }
    }
}
