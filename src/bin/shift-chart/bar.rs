use std::fmt;
use colored::Colorize;
use std::io::{stdout, Write};
use std::time::{Instant, Duration};
use terminal_size::{Width, Height, terminal_size};

use crate::ARROW_CHAR;

#[derive(Debug)]
pub struct ProgressBar {
    pub number_of_jobs: u64,
    pub completed_jobs: u64,
    pub action_message: String,
    is_on_terminal: bool,
    creation_instant: Instant,
    last_job_completed: Instant,
    last_job_duration: Duration,
    pub job_char: char,
}

impl ProgressBar {
    pub fn new( number_of_jobs: u64, action_message: String, job_char: char ) -> Self {
        let creation_instant = Instant::now();
        let last_job_completed = Instant::now();
        let last_job_duration = Duration::from_secs_f64(0.0);

        Self {
            number_of_jobs,
            completed_jobs: 0,
            action_message,
            is_on_terminal: false,
            creation_instant,
            last_job_completed,
            last_job_duration,
            job_char,
        }
    }
    
    pub fn job_completed( &mut self ) {
        self.last_job_duration = self.last_job_completed.elapsed();
        self.last_job_completed = Instant::now();
        self.completed_jobs += 1;
    }

    pub fn display( &mut self ) {
        if self.is_on_terminal {
            print!("\r");    
        } else {
            self.is_on_terminal = true
        }

        print!( "{}", &self );
        stdout().flush().unwrap(); // Flush to print immedialty
    }

    fn get_elapsed_string( &self ) -> String {
        let mut elapsed_string = String::new();

        let elapsed = self.creation_instant.elapsed();
        let elapsed_seconds = elapsed.as_secs() as f64;

        if elapsed_seconds >= 120.0 {
            elapsed_string.push_str( &get_clock_string(elapsed_seconds) );

        } else {
            let complete_seconds = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64 * 1e-9;

            let precision = match elapsed.as_secs() {
                0..=9 => 2,
                10..=60 => 1,
                _ => 0,
            };

            elapsed_string.push_str( &format!("{:.precision$}s", complete_seconds) );
        }

        elapsed_string
    }

    fn get_jobs_per_second_string( &self ) -> String {
        let elapsed = self.last_job_duration;
        let complete_seconds = elapsed.as_secs() as f64 + elapsed.subsec_nanos() as f64 * 1e-9;
        let mut jobs_per_second = 1.0 / complete_seconds;

        let unit = match jobs_per_second as u64 {
            0..=999 => "",
            _ => "k",
        };

        if jobs_per_second >= 1_000.0 {
            jobs_per_second /= 1_000.0;
        }

        format!( "{:.1}{}{}/s", jobs_per_second, unit, self.job_char )
    }
}

impl fmt::Display for ProgressBar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let terminal_width = get_terminal_width();
        let formatted_action_message = format!("{} {}", ARROW_CHAR.green().bold(), self.action_message);
        let progress_bar_size = (terminal_width as f64 * 0.5).round() as usize;
        let remaining_columns = terminal_width - self.action_message.chars().count() + 9; // Random 9 because rust counts colored's escaped chars to left align the progress bar
        let elapsed_time = self.get_elapsed_string();

        let ratio = self.completed_jobs as f64 / self.number_of_jobs as f64;
        let percentage = ( ratio * 100.0 ).round() as i64;
        let number_of_completed_chars = (progress_bar_size as f64 * ratio).round() as usize;

        let percentage_string = format!("{}%", percentage);
        let completed_chars = format!( "{:#<number_of_completed_chars$}", "");
        let progress_bar = format!( 
            "{} ({}) [{:-<progress_bar_size$}] {:<4}", 
            elapsed_time,
            self.get_jobs_per_second_string().bold(),
            completed_chars, 
            percentage_string
        ).bold();

        write!( f, "{} {:>remaining_columns$}", formatted_action_message, progress_bar )
    }
}

fn get_clock_string( elapsed_seconds: f64 ) -> String {
    let mut clock_string = String::new();
   
    let minutes = ( (elapsed_seconds / 60.0) % 60.0 ).floor();
    let seconds = elapsed_seconds % 60.0;

    if elapsed_seconds >= 3600.0 {
        let hours = (elapsed_seconds / 3600.0).floor();

        clock_string.push_str( &format!("{:02}:", hours) );
    }

    clock_string.push_str( &format!("{:02}:", minutes) );
    clock_string.push_str( &format!("{:02}", seconds) );

    clock_string
}

fn get_terminal_width() -> usize {
    let terminal_size = terminal_size();

    if let Some( (Width(width), Height(..)) ) = terminal_size {
        return width as usize
    } else {
        panic!("Unable to read terminal size")
    }
}
