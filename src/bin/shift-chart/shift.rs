use std::fmt;
use std::slice::Iter;
use calamine::DataType;
use serde::{Serialize, Serializer};

use crate::workbook::CellReader;

const REQUIRED_EVENINGS: u8 = 3;
const REQUIRED_MORNINGS: u8 = 3;
const REQUIRED_FASTTRACKS: u8 = 1;
const REQUIRED_AFTERNOONS: u8 = 1;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize)]
pub enum ShiftKind {
    Free,
    Empty,
    Evening,
    Morning,
    PostTurn,
    Vacation,
    FastTrack,
    Afternoon,
    Inability,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Shift {
    pub kind: ShiftKind,
    pub char: char,
    pub number: Option<u8>,
}

impl Shift {
    pub fn new() -> Self {
        Self::from(ShiftKind::Empty)
    }

    pub fn from_data_type( data_type: &DataType ) -> Self {
        let data_type_char = data_type.unwrap_string().chars().nth(0).unwrap();

        Self::from_char(data_type_char)
    }

    pub fn from_char( char: char ) -> Self {
        let kind = match char {
            'L' => ShiftKind::Free,
            ' ' => ShiftKind::Empty,
            'N' => ShiftKind::Evening,
            'C' => ShiftKind::Morning,
            'P' => ShiftKind::PostTurn,
            'V' => ShiftKind::Vacation,
            'F' => ShiftKind::FastTrack,
            'T' => ShiftKind::Afternoon,
            'I' => ShiftKind::Inability,
            _ => panic!("No corresponding shift kind was found for char {}", char)
        };

        Self::from(kind)
    }

    pub fn from( kind: ShiftKind ) -> Self {
        let char = match kind {
            ShiftKind::Free => 'L',
            ShiftKind::Empty => ' ',
            ShiftKind::Evening => 'N',
            ShiftKind::Morning => 'C',
            ShiftKind::PostTurn => 'P',
            ShiftKind::Vacation => 'V',
            ShiftKind::FastTrack => 'F',
            ShiftKind::Afternoon => 'T',
            ShiftKind::Inability => 'I',
        };

        let number = match kind {
            ShiftKind::Evening | ShiftKind::Morning => Some(0),
            _ => None,
        };

        Self { kind, char, number }
    }

    pub fn is_empty( &self ) -> bool {
        match self.kind {
            ShiftKind::Empty => true,
            _ => false,
        }
    }

    pub fn is_free( &self ) -> bool {
        match self.kind {
            ShiftKind::Evening | ShiftKind::Morning | ShiftKind::FastTrack | ShiftKind::Afternoon => false,
            _ => true,
        }
    }
}

impl ShiftKind {
    pub fn iterator() -> Iter<'static, ShiftKind> {
        static KINDS: [ShiftKind; 9] = [
            ShiftKind::Free,
            ShiftKind::Empty,
            ShiftKind::Evening,
            ShiftKind::Morning,
            ShiftKind::PostTurn,
            ShiftKind::Vacation,
            ShiftKind::FastTrack,
            ShiftKind::Afternoon,
            ShiftKind::Inability,
        ];

        KINDS.iter()
    }

    pub fn required( &self ) -> u8 {
        match self {
            ShiftKind::Evening => REQUIRED_EVENINGS,
            ShiftKind::Morning => REQUIRED_MORNINGS,
            ShiftKind::FastTrack => REQUIRED_FASTTRACKS,
            ShiftKind::Afternoon => REQUIRED_AFTERNOONS,
            _ => 0,
        }
    }

    pub fn duration( &self ) -> u8 {
        match self {
            ShiftKind::Evening | ShiftKind::Morning | ShiftKind::FastTrack => 12,
            ShiftKind::Afternoon => 6,
            _ => 0,
        }
    }
}

impl fmt::Display for Shift {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let number = match self.number {
            Some(number) => number.to_string(),
            None => match self.kind {
                ShiftKind::FastTrack | ShiftKind::PostTurn => "T".to_string(),
                _ => " ".to_string(),
            },
        };

        let mut shift_string = String::new();
        shift_string.push(self.char);

        if self.number.is_some() {
            shift_string.push_str(&number)

        } else if self.kind == ShiftKind::FastTrack {
            shift_string.push('T')
        }

        write!( f, "{}", shift_string )
    }
}

impl Serialize for Shift {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str( &format!("{}", self) )
    }
}
