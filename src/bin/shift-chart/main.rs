#![feature(io_error_more)]

mod bar;
mod cli;
mod shift;
mod chart;
mod config;
mod employee;
mod workbook;
mod exception;
mod arguments;
mod statistics;
mod calibration;

mod api;
use std::env;
use chart::Chart;
use config::Config;
use bar::ProgressBar;
use calibration::Calibrator;

pub const NAME: &str        = "Shift Chart solver";
pub const VERSION: &str     = "0.4.0";
pub const DESCRIPTION: &str = "a clinic shift chart solution generator written in Rust";
pub const COMMAND: &str     = "shift-chart";

const ARROW_CHAR: &'static str = "❯";

const CARRIED_DAYS: usize = 3;

fn display_solutions( solved_charts: &mut Vec<Chart>, config: &Config ) {
    solved_charts.sort_by( 
        |a, b| a.get_balance_deviation().partial_cmp(&b.get_balance_deviation()).unwrap()
    );

    let solutions_to_display: Vec<Chart> = Vec::from( &solved_charts[..config.solutions as usize] );
    let mut solved_charts_to_display: Vec<Chart> = Vec::new();

    for mut chart in solutions_to_display {
        chart.finish_solving(&config);
        solved_charts_to_display.push( chart );
    }

    if config.be_quiet {
        workbook::save_workbook(solved_charts_to_display, config);
    
    } else {
        let displaying_message = format!( 
            "Displaying `{}` solution{} from a total of `{}` calulated:\n",
            &config.solutions,
            if &config.solutions > &1 {"s"} else {""},
            solved_charts.len(),
        );

        for (index, chart) in solved_charts_to_display.into_iter().enumerate() {
            cli::inform(&displaying_message);
            cli::warn( &format!("Chart solution `#{}`", index + 1) );
            println!("{}", chart);
        }
    }
}

fn solve_chart( config: &mut Config, progress_bar: &mut ProgressBar ) {
    let mut solved_charts: Vec<Chart> = Vec::new();
    let mut shift_chart = Chart::from_config(&config);
    
    config.calibrate_scores(progress_bar);

    let _ = shift_chart.solve(&mut solved_charts, &config, progress_bar); 
    
    display_solutions(&mut solved_charts, &config);
}

fn main() {
    let mut config = Config::new();
    arguments::read(env::args().collect(), &mut config);

    if config.run_api {
        api::main().unwrap();
        
    } else {
        let mut progress_bar = ProgressBar::new(config.calculations, "Calculation solutions...".to_string(), 'S');
        solve_chart(&mut config, &mut progress_bar);
    }
}
