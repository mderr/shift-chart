use std::fs;
use std::net::IpAddr;

use super::utils;
use super::Settings;
use crate::bar::ProgressBar;

#[derive(Debug)]
pub struct Client {
    pub address: IpAddr,
    pub settings: Settings,
    pub workbook_filepath: String,
    pub progress_bar: Option<ProgressBar>,
}

impl Client {
    pub fn new( ip_address: IpAddr ) -> Self {
        Client {
            address: ip_address,
            settings: Settings::new(),
            workbook_filepath: String::from(""),
            progress_bar: None,
        }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        let tmp_path = utils::get_tmp_path(self.address);

        fs::remove_dir_all(tmp_path).unwrap();
    }
}
