use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub worksheet_name: String,
    pub calculations: u64,
    pub solutions: u64,
}

impl Settings {
    pub fn new() -> Self {
        Self {
            worksheet_name: "".to_string(),
            calculations: 10_000,
            solutions: 1,
        }
    }
}
