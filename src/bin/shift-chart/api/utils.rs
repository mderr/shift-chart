use std::fs;
use std::path::Path;
use std::net::IpAddr;

use super::client::Client;

const TMP_PATH: &'static str = "tmp";

pub fn get_tmp_path( address: IpAddr ) -> String {
    let tmp_dir_path = format!("{}/{}/", TMP_PATH, address);

    if !Path::new(&tmp_dir_path).exists() {
        fs::create_dir_all(&tmp_dir_path).expect("Creating tmp directory");
    }

    tmp_dir_path
}

pub trait Finder {
    fn find_index( &self, address: IpAddr ) -> usize ;
}

impl Finder for Vec<Client> {
    fn find_index( &self, address: IpAddr ) -> usize {
        for (index, client) in self.iter().enumerate() {
            if client.address == address {
                return index
            }
        }

        panic!("Client not found")
    }
}
