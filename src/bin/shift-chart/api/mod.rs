mod utils;
mod client;
mod settings;

use uuid::Uuid;
use utils::Finder;
use client::Client;
use std::io::Write;
use std::path::Path;
use actix_web::http;
use calamine::Reader;
use serde::Serialize;
use std::net::IpAddr;
use std::sync::Mutex;
use settings::Settings;
use actix_files::NamedFile;
use actix_multipart::Multipart;
use futures_util::TryStreamExt as _;
use actix_web::{get, post, web, middleware, App, HttpResponse, HttpServer, Responder, HttpRequest};

use crate::workbook;
use crate::config::Config;
use crate::bar::ProgressBar;

const SOLVED_FILENAME: &'static str = "solución";
const XLSX_MIME: &'static str = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

#[derive(Debug)]
struct State {
    clients: Mutex<Vec<Client>>
}

fn register_client( ip_address: IpAddr, data: &web::Data<State> ) {
    let mut clients = data.clients.lock().unwrap();

    if !clients.iter().any( |c| c.address == ip_address ) {
        clients.push( Client::new(ip_address) )
    }
}

#[get("/health")]
async fn health() -> impl Responder {
    HttpResponse::Ok().body("API up and running")
}

#[post("/upload-settings")]
async fn upload_settings( req: HttpRequest, req_body: String,  data: web::Data<State> ) -> impl Responder {
    let address = req.peer_addr().unwrap().ip();
    register_client(address, &data);

    let mut clients = data.clients.lock().unwrap();
    let client_index = clients.find_index(address);

    let info: Settings = serde_json::from_str(&req_body).unwrap();

    clients[client_index].settings.worksheet_name = String::from( &info.worksheet_name );
    clients[client_index].settings.calculations = info.calculations;
    clients[client_index].settings.solutions = info.solutions;
    clients[client_index].progress_bar = Some(
        ProgressBar::new(info.calculations, "".to_string(), 'S')
    );

    HttpResponse::Ok().body("Info received")
}

#[post("/upload-workbook")]
async fn upload_workbook( req: HttpRequest, mut payload: Multipart, data: web::Data<State> ) -> impl Responder {
    let address = req.peer_addr().unwrap().ip();
    register_client(address, &data);

    let mut filepath = utils::get_tmp_path(address);

    // iterate over multipart stream
    while let Some(mut field) = payload.try_next().await.unwrap() {
        // A multipart/form-data stream has to contain `content_disposition`
        let content_disposition = field.content_disposition();

        if field.content_type() != &XLSX_MIME.parse::<mime::Mime>().unwrap() {
            return HttpResponse::BadRequest().body("File type is not xlsx")
        }

        let filename = content_disposition
            .get_filename()
            .map_or_else(|| Uuid::new_v4().to_string(), sanitize_filename::sanitize);

        filepath.push_str( &filename );
        let path = filepath.clone();

        // File::create is blocking operation, use threadpool
        let mut f = web::block(|| std::fs::File::create(path)).await.unwrap().unwrap();

        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.try_next().await.unwrap() {
            // filesystem operations are blocking, we have to use threadpool
            f = web::block(move || f.write_all(&chunk).map(|_| f)).await.unwrap().unwrap();
        }
    }

    let mut clients = data.clients.lock().unwrap();
    let client_index = clients.find_index(address);

    clients[client_index].workbook_filepath = filepath.clone();

    let workbook = workbook::get_workbook(&filepath).unwrap();
    let worksheet_names: Vec<String> = workbook.sheet_names().to_vec();

    HttpResponse::Ok().json(worksheet_names)
}

#[get("/get-solution")]
async fn get_solution( req: HttpRequest, data: web::Data<State> ) -> impl Responder {
    let address = req.peer_addr().unwrap().ip();
    let mut clients = data.clients.lock().unwrap();

    if !clients.iter().any( |c| c.address == address ) {
        return HttpResponse::BadRequest().body("This remote address has not sent any info")
    }

    let client_index = clients.find_index(address);

    let mut config = Config::from(
        clients[client_index].settings.calculations,
        clients[client_index].settings.solutions,
        clients[client_index].workbook_filepath.clone(),
        Some( clients[client_index].settings.worksheet_name.clone() ),
    );

    config.be_quiet = true;

    let progress_bar = clients.get_mut(client_index).unwrap().progress_bar.as_mut().unwrap();

    crate::solve_chart(&mut config, progress_bar);

    let extension = Path::new(&clients[client_index].workbook_filepath).extension().unwrap().to_str().unwrap();
    let solution_filepath = format!("{}{}.{}", utils::get_tmp_path(address), SOLVED_FILENAME, extension);

    NamedFile::open_async(solution_filepath).await.unwrap().into_response(&req)
}

#[get("/check-progress")]
async fn check_progress( _req: HttpRequest, _data: web::Data<State> ) -> impl Responder {
    // TODO: make this work somehow

    #[derive(Serialize)]
    struct ClientProgress {
        calculated: u64,
        total: u64,
    }

    HttpResponse::Ok().json( ClientProgress {calculated: 1, total: 1} )
}

#[get("/drop-client")]
async fn drop_client( req: HttpRequest,data: web::Data<State> ) -> impl Responder {
    let address = req.peer_addr().unwrap().ip();
    let mut clients = data.clients.lock().unwrap();
    
    if clients.iter().any( |c| c.address == address ) {
        let client_index = clients.find_index(address);
        clients.remove(client_index);
    }

    HttpResponse::Ok().body("Client dropped")
}

#[actix_web::main]
pub async fn main() -> std::io::Result<()> {
    let state = web::Data::new( State {
        clients: Mutex::new(Vec::new())
    } );

    HttpServer::new(move || {
        App::new()
            .wrap(middleware::DefaultHeaders::new()
                .add(("Access-Control-Allow-Origin", "*"))
                .add( (http::header::ACCESS_CONTROL_ALLOW_CREDENTIALS, "true") )
                .add( (http::header::ACCESS_CONTROL_ALLOW_METHODS, "POST, GET, PUT, DELETE") )
                .add( (http::header::ACCESS_CONTROL_ALLOW_HEADERS, "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization") )
            )
            .app_data( state.clone() )
            .service(health)
            .service(upload_settings)
            .service(upload_workbook)
            .service(get_solution)
            .service(check_progress)
            .service(drop_client)
    })
    .bind("localhost:8080")?
    .run()
    .await
}
