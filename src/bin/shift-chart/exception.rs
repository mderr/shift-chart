use std::process::exit;
use std::io::{Error, ErrorKind};

use crate::cli;

pub struct Exception {
    subject: &'static str,
    action: &'static str,
    suggestion: &'static str,
    exit_code: i32,
}

const GENERIC_ERROR: Exception = Exception {
    subject: "An error", action: "has occured", suggestion: "and it doesn't have a custom message",
    exit_code: 1
};
const OPTION_ERROR: Exception = Exception {
    subject: "Option", action: "was not recognized", suggestion: "try `--help` to see all options",
    exit_code: 64
};
const FILE_ERROR: Exception = Exception {
    subject: "Path", action: "could not be found", suggestion: "please check the spelling",
    exit_code: 66
};
const INTERNAL_ERROR: Exception = Exception {
    subject: "Internal development error when trying to", action: "was raised", suggestion: "please report",
    exit_code: 70
};
const WORKSHEET_ERROR: Exception = Exception {
    subject: "The worksheet", action: "couldn't be found in this workbook", suggestion: "please check the spelling",
    exit_code: 1
};
const PERMISSION_ERROR: Exception = Exception {
    subject: "Permission for", action: "was denied", suggestion: "did you mean to run as root?",
    exit_code: 77
};

fn get_exception_message( exception: &Exception, subject_name: &str ) -> String {
    let exception_message =  format!( "{subject} `{subject_name}` {action}, {suggestion}",
        subject = exception.subject,
        subject_name = subject_name,
        action = exception.action,
        suggestion = exception.suggestion,
    );

    exception_message
}

fn handle_error(  error: Error, subject_name: &str ) -> ! {
    let exception: Exception = match error.kind() {
        ErrorKind::NotFound => FILE_ERROR,
        ErrorKind::PermissionDenied => PERMISSION_ERROR,
        
        _ => {
            cli::alert( get_exception_message(&GENERIC_ERROR, &error.to_string()).as_str() );
            exit( GENERIC_ERROR.exit_code )
        },
    };

    cli::alert( get_exception_message(&exception, subject_name).as_str() );
    exit( exception.exit_code )
}

pub enum CustomErrorKind {
    InvalidOption,
    Internal,
    Worksheet,
}

pub fn _raise_error( error_kind: ErrorKind, subject_name: &str ) -> ! {
    let error = Error::from(error_kind);

    handle_error(error, subject_name);
}

pub fn raise( custom_error_kind: CustomErrorKind, subject_name: &str ) -> ! {
    let exception: Exception = match custom_error_kind {
        CustomErrorKind::InvalidOption => OPTION_ERROR,
        CustomErrorKind::Internal => INTERNAL_ERROR,
        CustomErrorKind::Worksheet => WORKSHEET_ERROR,
    };

    cli::alert( get_exception_message(&exception, subject_name).as_str() );

    exit( exception.exit_code )
}

pub fn exit_with_success() {
    exit(0)
}

pub trait Handler<T> {
    fn handle( self, subject_name: &str ) -> T;
}

impl<T> Handler<T> for Result<T, Error> {
    fn handle( self, subject_name: &str ) -> T {
        let result: T = match self {
            Ok(ok) => ok,
            Err(err) => handle_error(err, subject_name),
        };

        result
    }
}
