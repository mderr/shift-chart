use std::fs::File;
use std::path::Path;
use std::io::BufReader;
use std::process::Command;
use calamine::{DataType, Range, Xlsx, Reader, open_workbook};

use crate::CARRIED_DAYS;
use crate::chart::Chart;
use crate::shift::Shift;
use crate::config::Config;
use crate::exception::Handler;
use crate::exception::{self, CustomErrorKind};

pub fn get_workbook<T: ?Sized>( file_path: &T ) -> Result<Xlsx<BufReader<File>>, std::io::Error>
where T: AsRef<Path> {
    let workbook: Xlsx<_> = open_workbook(file_path).unwrap();

    Ok(workbook)
}

pub fn save_workbook( solved_charts: Vec<Chart>, config: &Config ) {
    let mut shift_chart_csvs: Vec<String> = Vec::new();

    let python_writer = include_bytes!("writer.py");

    for chart_solution in solved_charts {
        let csv = chart_solution.get_csv().handle("");
        shift_chart_csvs.push( csv );
    }

    let mut write = Command::new("python")
        .arg("-c")
        .arg( String::from_utf8_lossy(python_writer).to_string() )
        .arg( config.workbook_filepath.clone() )
        .args(shift_chart_csvs)
        .spawn()
        .expect("failed to execute process");

    write.wait().unwrap();
}

pub trait WorkbookReader {
    fn get_worksheet( &mut self, worksheet_name: &str ) -> Range<DataType>;
}

impl WorkbookReader for Xlsx<BufReader<File>> {
    fn get_worksheet(&mut self, worksheet_name: &str) -> Range<DataType> {
        let worksheet = self.worksheet_range(worksheet_name);

        match worksheet {
            Some(result) => result.unwrap(),
            None => exception::raise(CustomErrorKind::Worksheet, worksheet_name)
        }
    }
}

pub trait WorksheetReader {
    fn get_bounds( &self ) -> (usize, usize, usize);
    fn get_position_of_string( &self, string: &str ) -> (usize, usize);
    fn get_shifts_inside_bounds( &self, row_index: usize, month_start: usize, month_end: usize ) -> Vec<Shift>;
    fn get_non_working_indices( &self ) -> Vec<usize>;
}

impl WorksheetReader for Range<DataType> {
    fn get_bounds( &self ) -> (usize, usize, usize) {
        let mut month_start_index: Option<usize> = None;
        let mut month_end_index: Option<usize> = None;
        let mut employee_end_index: Option<usize> = None;
    
        let first_row = self.rows().nth(3).unwrap();
     
        for (index, column) in first_row.iter().enumerate() {
            if matches!(column, DataType::Float(..)) {
                if month_start_index.is_none() {
                    month_start_index = Some(index + 1);
                    
                } else {
                    month_end_index = Some(index - 1);
                    break
                }
            }
        }
    
        for (index, row) in self.rows().skip(3).into_iter().enumerate() {
            if matches!(row[0], DataType::Empty) {
                employee_end_index = Some(index + 2);
                break
            }
        }
    
        ( month_start_index.unwrap(), month_end_index.unwrap(), employee_end_index.unwrap() )
    }

    fn get_position_of_string( &self, string: &str ) -> (usize, usize) {
        let x: usize;
        let y: usize;

        for (row_index, row) in self.rows().into_iter().enumerate() {
            for (column_index, column) in row.iter().enumerate() {
                if matches!(column, DataType::String(..)) {
                    let column_string = column.unwrap_string();
        
                    if column_string == string {
                        x = column_index;
                        y = row_index;

                        return (x, y)
                    }
                }   
            }
        }
    
        panic!("The string couldn't be found in worksheet")
    }

    fn get_shifts_inside_bounds( &self, row_index: usize, month_start: usize, month_end: usize ) -> Vec<Shift> {
        let row = &self.rows().skip(3).nth(row_index).unwrap()[month_start+CARRIED_DAYS..=month_end];
        let mut shifts: Vec<Shift> = Vec::new();
        
        for cell in row {
            let shift: Shift;
    
            if matches!(cell, DataType::String(..)) {
                shift = Shift::from_data_type(cell);
            } else {
                shift = Shift::new();   
            }
    
            shifts.push(shift);
        }

        shifts
    }

    fn get_non_working_indices( &self ) -> Vec<usize> {
        let mut non_working_indices: Vec<usize> = Vec::new();
    
        let (month_start_index, _, _) = &self.get_bounds();
    
        for (index, column) in self.rows().nth(0).unwrap().iter().enumerate() {
            if column == &DataType::String( String::from("D") ) {
                non_working_indices.push(index - month_start_index);
            }
        }
    
        non_working_indices
    }
}

pub trait CellReader {
    fn unwrap_string( &self ) -> String;
    fn unwrap_float( &self ) -> f64;
}

impl CellReader for DataType {
    fn unwrap_string( &self ) -> String {
        match self {
            DataType::String(string) => string.to_string(),
            _ => panic!("No implementation available for this DataType"),
        }
    }

    fn unwrap_float(&self) -> f64 {
        match self {
            DataType::Float(float) => *float,
            _ => panic!("No implementation available for this DataType"),
        }
    }
}
