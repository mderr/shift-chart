import os
import sys
import pathlib
import pandas as pd
from io import StringIO
from openpyxl import load_workbook

BEGINING_CELL_ROW    = 4
BEGINING_CELL_COLUMN = 6

WORKSHEET_NAME_FORMAT = "Solución {}"
SOLUTION_FILENAME_FORMAT = "{}/solución{}"

def write_solution_to_worksheet( solution_worksheet, chart_solution_df ):
     for employee_index in range(len(chart_solution_df)):
        for x_i, x in enumerate(chart_solution_df.loc[employee_index]):
            shift =  chart_solution_df.loc[employee_index, str(x_i + 1)]

            solution_worksheet.cell(row=employee_index + BEGINING_CELL_ROW, column=x_i + BEGINING_CELL_COLUMN, value=shift)

def get_solution_workbook( chart_solutions, original_workbook_path ):
    workbook = load_workbook(original_workbook_path)

    original_worksheet = workbook.active

    for index, chart_sol in enumerate(chart_solutions):
        solution_worksheet = workbook.copy_worksheet(original_worksheet)
        solution_worksheet.title = WORKSHEET_NAME_FORMAT.format(index + 1)

        write_solution_to_worksheet(solution_worksheet, chart_sol)

    workbook.save( SOLUTION_FILENAME_FORMAT.format(
        os.path.dirname(original_workbook_path), 
        pathlib.Path(original_workbook_path).suffix
    ) )
    
def main():
    chart_solutions = []
    original_workbook_path = str( sys.argv[1] )

    for csv in sys.argv[2:]:
        chart_solutions.append( pd.read_csv(StringIO(csv), sep=",") )

    get_solution_workbook( chart_solutions, original_workbook_path )
    
if __name__ == "__main__":
    main()