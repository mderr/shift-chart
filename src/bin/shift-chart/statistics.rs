pub trait Stats {
    fn get_average( &self ) -> f64;
    fn get_variance( &self ) -> f64;
    fn get_stdev( &self ) -> f64;
}

impl<T> Stats for Vec<T>
    where T: Into<f64> + Copy {
    fn get_average( &self ) -> f64 {
        let mut sum: f64 = 0.0;

        for element in self.iter() {
            let element_float = *element;

            sum += element_float.into();
        }

        sum / self.len() as f64
    }

    fn get_variance( &self ) -> f64 {
        let mut numerator = 0.0;
        let average = self.get_average();
        
        for element in self.iter() {
            let element_float = *element;

            let distance = element_float.into() - average;

            numerator += distance * distance;
        }

        numerator / self.len() as f64
    }

    fn get_stdev( &self ) -> f64 {
        let variance = self.get_variance();

        (variance).sqrt()
    }
}