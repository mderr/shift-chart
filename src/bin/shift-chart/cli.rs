use colored::Colorize;
use std::io::{stdout, prelude::*};

use crate::arguments;
use crate::ARROW_CHAR;
use crate::exception::{self, Handler};

const FORMAT_CHAR: char = '`';

#[derive(PartialEq)]
enum MessageColor {
    Green,
    Red,
    Yellow,
    Purple,
}

fn get_highlighted_text( formated_text: &str, message_color: MessageColor ) -> String {
    let mut text_string = String::from(formated_text);
    let get_format_chars = |string: &String| { string.matches(FORMAT_CHAR).count() };

    if get_format_chars(&text_string) % 2 != 0 {
        exception::raise(exception::CustomErrorKind::Internal, "get highlighted text (format chars not even)");
    }

    while get_format_chars(&text_string) > 0 {
        let words: Vec<&str> = text_string.splitn(3, FORMAT_CHAR).collect();

        let highlighted_word = match message_color {
            MessageColor::Green  => Colorize::green( words[1] ),
            MessageColor::Red    => Colorize::red( words[1] ),
            MessageColor::Yellow => Colorize::yellow( words[1] ),
            MessageColor::Purple => Colorize::purple( words[1] ),
        };

        text_string = format!( "{}{}{}", words[0].white(), highlighted_word, words[2].white() );
    }

    text_string
}

fn print_message( message: &str, message_color: MessageColor, print_newline: bool ) {
    let message = format!("`{}` {}", ARROW_CHAR, message);
    let formatted_message = get_highlighted_text(&message, message_color);

    print!("{formatted_message}");

    stdout().flush().handle("Flushing stdout"); // Flush to print immedialty

    if print_newline {
        println!();
    }
}

pub fn inform( message: &str ) {
    print_message( message, MessageColor::Green, true );
}

pub fn alert( message: &str ) {
    print_message( message, MessageColor::Red, true );
}

pub fn warn( message: &str ) {
    print_message( message, MessageColor::Yellow, true );
}

pub fn show_help() {
    let name_message = format!( "`{name}` {version}, {description}",
        name = crate::NAME,
        version = crate::VERSION.bold(),
        description = crate::DESCRIPTION,
    );

    let usage_message = format!( "`Usage:` {command} {options} {folder}\n",
        command = crate::COMMAND.bold(),
        options = "[Options]...".purple(),
        folder = "<Shift chart filepath>".blue(),
    );

    let options_title = "`Options:`";

    print_message(&name_message, MessageColor::Yellow, true);
    print_message(&usage_message, MessageColor::Red, true);
    print_message(options_title, MessageColor::Purple, true);

    for flag in arguments::FLAGS {
        let tabs = match flag.long.chars().count() {
            0..=8 => "\t\t",
            _ => "\t",
        };

        println!( "  -{short}, --{long}{tabs}{description}",
            short = flag.short.to_string().bold(),
            long = flag.long.bold(),
            description = flag.description,
        );
    }

    exception::exit_with_success()
}

pub fn show_version() {
    let version_message = format!("`{name}` version `{version}`", 
        name = crate::NAME,
        version = crate::VERSION
    );

    inform(&version_message);

    exception::exit_with_success()
}
