use csv;
use std::fmt;
use colored::Colorize;
use std::collections::HashMap;
use std::io::{Error, ErrorKind};
use calamine::{Range, DataType, Reader};

use crate::bar;
use crate::config::Config;
use crate::statistics::Stats;
use crate::exception::Handler;
use crate::employee::Employee;
use crate::shift::{Shift, ShiftKind};
use crate::{CARRIED_DAYS, ARROW_CHAR};
use crate::workbook::{WorksheetReader, CellReader, self, WorkbookReader};

#[derive(Clone)]
pub struct Chart {
    pub chart: Vec<Vec<Shift>>,
    pub max_contract: u16,
    pub days_of_the_month: usize,
    pub non_working_indices: Vec<usize>,
    pub employee_vector: Vec<Employee>,
    false_shifts: u32,
}

impl fmt::Display for Chart {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let employee_name_space = self.get_max_employee_name_len() + 4;
        let shift_deviations = self.get_shift_deviations();

        let arrowed = |str: &str| {
            format!("{} {}", ARROW_CHAR.bold(), str)
        };

        write!( f, "\t{:width$}", "", width = employee_name_space )?;

        for i in 1..=self.days_of_the_month {
            let day_number = match self.non_working_indices.contains( &(i-1+CARRIED_DAYS) ) {
                true => i.to_string().yellow(),
                false => i.to_string().white(),
            };

            write!(f, " {:>2}", day_number.bold())?;
        }

        write!( f, "    {}", " Ev Mo Fa Af    Balance\n".bold() )?;

        for (employee_index, row) in self.chart.iter().enumerate() {
            let employee = &self.employee_vector[employee_index];

            let employee_balance = if employee.get_balance() < 0 {
                format!( "{}", employee.get_balance() ).red()
            } else {
                format!( "+{}", employee.get_balance() ).green()
            };

            write!( f, "\t{:width$}", employee.name.bold(), width=employee_name_space )?;

            for cell in &row[CARRIED_DAYS..] {
                let cell_string = format!("{}", cell);
                write!( f, "{}{:2}", "|".black(), cell_string )?;
            }

            write!( f, "{}", "|".black() )?;

            write!( f, "    " )?;
            write!( f, "{:<2} ", employee.shift_counts.get(&ShiftKind::Evening).unwrap() )?;
            write!( f, "{:<2} ", employee.shift_counts.get(&ShiftKind::Morning).unwrap() )?;
            write!( f, "{:<2} ", employee.shift_counts.get(&ShiftKind::FastTrack).unwrap() )?;
            write!( f, "{:<2} ", employee.shift_counts.get(&ShiftKind::Afternoon).unwrap() )?;
            write!( f, "   " )?;
            write!( f, "{:^7} ", format!("{}/{}", employee.worked_hours, employee.contract_hours) )?;
            write!( f, "{:<2}\n", employee_balance.bold() )?;
        }

        write!( f, "\n\t{}\n\n", "[ Chart statistics ]".purple().bold() )?;

        write!( f, "\t{}      {}             {}\n", arrowed("Shift deviations").blue().bold(), arrowed("Balance").blue().bold(), arrowed("Employee").blue().bold() )?;

        write!( f, "\t  {} {:.2}        ", "Evenings:  ".bold(), shift_deviations.0 )?;
        write!( f, "{} {:.2}      ", "Average:  ".bold(), self.get_balances().get_average() )?;
        write!( f, "{} {:.2}\n", "Shift balance deviation:".bold(), self.get_shift_balance_deviation() )?;

        write!( f, "\t  {} {:.2}        ", "Mornings:  ".bold(), shift_deviations.1 )?;
        write!( f, "{} {:.2}\n", "Negatives:".bold(), self.get_balances().iter().filter(|x| *x < &0).count() )?;

        write!( f, "\t  {} {:.2}        ", "FastTracks:".bold(), shift_deviations.2 )?;
        write!( f, "{} {:.2}\n", "Deviation:".bold(), self.get_balance_deviation() )?;

        write!( f, "\t  {} {:.2}\n", "Afternoons:".bold(), shift_deviations.3 )?;

        Ok(())
    }
}

impl Chart {
    pub fn from_config( config: &Config ) -> Self {
        let mut workbook = workbook::get_workbook(&config.workbook_filepath).handle("Getting excel workbook");

        let worksheet_name =  match &config.worksheet_name {
            Some(name) => String::from(name),
            None => {
                let workbook_copy = workbook::get_workbook(&config.workbook_filepath).unwrap();
                workbook_copy.sheet_names().last().unwrap().to_string()
            },
        };

        Self::from( &workbook.get_worksheet(&worksheet_name) )
    }

    pub fn from( worksheet: &Range<DataType> ) -> Self {
        let worksheet_rows: Vec<&[DataType]> = worksheet.rows().collect();
        let mut employee_vector: Vec<Employee> = Vec::new();

        let (month_start, month_end, employee_end) = worksheet.get_bounds();

        let (contract_hours_index, _) = worksheet.get_position_of_string("CONTRATO");
        let (name_index, _) =  worksheet.get_position_of_string("NOMBRES");

        let mut max_contract = 0;

        let chart_width = (month_end - month_start) + 1;
        let chart_height = (employee_end - 3) + 1;
        let days_of_the_month = chart_width - CARRIED_DAYS;

        let non_working_indices = worksheet.get_non_working_indices();
        
        let mut chart: Vec<Vec<Shift>> = vec![ vec![Shift::new(); chart_width]; chart_height ];

        for (x, row) in worksheet_rows[3..=employee_end].iter().enumerate() {
            let employee_assigned_shifts = worksheet.get_shifts_inside_bounds(x, month_start, month_end);
            let employee_contract_hours = ( row[contract_hours_index].unwrap_float() ).round() as u16;
            let employee_name = row[name_index].unwrap_string();

            let mut employee = Employee::new(x, employee_name, employee_contract_hours);

            employee.count_assigned_shifts(employee_assigned_shifts);
            employee_vector.push(employee);


            if employee_contract_hours > max_contract {
                max_contract = employee_contract_hours;
            }

            for (y, column) in row[month_start..=month_end].iter().enumerate() {
                if matches!(column, DataType::String(..)) {
                    chart[x][y] = Shift::from_data_type(column);
                }   
            }
        }

        Self {
            chart,
            max_contract,
            days_of_the_month,
            non_working_indices,
            employee_vector,
            false_shifts: 0,
        }
    }

    pub fn get_csv( &self ) -> Result<String, Error> {
        let mut writer = csv::Writer::from_writer(vec![]);
        let mut header = Vec::new();
        
        for i in 1..=self.days_of_the_month {
            header.push(i.to_string());
        }

        writer.write_record(&header)?;
        
        for employee in &self.chart {
            writer.serialize( &employee[CARRIED_DAYS..] )?;
        }

        let inner = &writer.into_inner().unwrap();
        let data = String::from_utf8_lossy(inner).to_string();

        Ok(data)
    }

    pub fn set_flags_for_day( &mut self, day_index: usize ) {
        for employee in &mut self.employee_vector {
            employee.reset_flags();

            let last_three_days = [ 
                &self.chart[employee.index][day_index - 1],
                &self.chart[employee.index][day_index - 2],
                &self.chart[employee.index][day_index - 3],
            ];

            if !self.chart[employee.index][day_index].is_empty(){
                employee.is_assigned = true;
            }

            if last_three_days[0].kind == ShiftKind::Evening {
                employee.is_resting = true;

                if last_three_days[1].kind == ShiftKind::Evening {
                    employee.evening_blocked = true;
                }
            }

            if !last_three_days.iter().any( |x| x.is_free() ) {
                employee.is_overworked = true;
            }
        }
    }

    fn get_max_shift_count( &self, shift: &ShiftKind ) -> u8 {
        let mut max_shift_count = 0;
    
        for employee in &self.employee_vector {
            let employee_shift_count = *employee.shift_counts.get(&shift).unwrap();
    
            if employee_shift_count > max_shift_count {
                max_shift_count = employee_shift_count;
            }
        }
    
        max_shift_count
    }

    pub fn set_scores_for_day( &mut self, day_index: usize, config: &Config ) {
        let remaining_days: f32 = self.chart[0][day_index..].len() as f32;
        let mut max_afternoons = self.get_max_shift_count(&ShiftKind::Afternoon);
        let mut max_fasttracks = self.get_max_shift_count(&ShiftKind::FastTrack);

        if max_afternoons == 0 {
            max_afternoons = 1
        }

        if max_fasttracks == 0 {
            max_fasttracks = 1
        }

        for employee in &mut self.employee_vector {
            let mut assignable_days: f32 = 0.0;

            for shift in &self.chart[employee.index][day_index..] {
                if shift.is_empty() {
                    assignable_days += 1.0;
                }
            }

            let availability_score = ( 1.0 - (assignable_days / remaining_days) ) * config.availability_score_bias;
            let contract_score = ( employee.contract_hours as f32 / self.max_contract as f32 ) * config.contract_score_bias;
            let worked_score = ( 1.0 - (employee.worked_hours as f32 / employee.contract_hours as f32) ) * config.worked_score_bias;

            let afternoon_balance_score = (*employee.shift_counts.get(&ShiftKind::Afternoon).unwrap() as f32 / max_afternoons as f32) * config.afternoon_balance_bias;
            let afternoon_score = ( (1.0 - contract_score) - afternoon_balance_score ) * config.afternoon_score_bias + ( 1.0 - availability_score );

            let fasttrack_score = 1.0 - (*employee.shift_counts.get(&ShiftKind::FastTrack).unwrap() as f32 / max_fasttracks as f32);

            employee.availability_score = availability_score;
            employee.contract_score = contract_score;
            employee.worked_score = worked_score;
            employee.afternoon_score = afternoon_score;
            employee.fasttrack_score = fasttrack_score;
        }
    }

    pub fn get_days_shifts( &self, day_index: usize ) -> (u8, u8, u8, u8) {
        let (mut evenings, mut mornings, mut fasttracks, mut afternoons) = (0, 0, 0, 0);
        
        for row in &self.chart {
            let day_shift = row[day_index];

            match day_shift.kind {
                ShiftKind::Evening => evenings += 1,
                ShiftKind::Morning => mornings += 1,
                ShiftKind::FastTrack => fasttracks += 1,
                ShiftKind::Afternoon => afternoons += 1,
                _ => (),
            }
        }

        (evenings, mornings, fasttracks, afternoons )
    }
    
    pub fn get_balances( &self ) -> Vec<i16> {
        let mut balances: Vec<i16> = Vec::new();

        for employee in &self.employee_vector {
            balances.push( employee.get_balance() );
        }

        balances
    }

    fn get_employees_with_shift( &self, day_index: usize, shift_kind: ShiftKind ) -> Vec<Employee> {
        let mut employees_with_shift: Vec<Employee> = Vec::new();

        for employee in &self.employee_vector {
            if &self.chart[employee.index][day_index].kind == &shift_kind {
                employees_with_shift.push( employee.clone() );
            }
        }

        employees_with_shift
    }

    pub fn assign_best_fastrack( &mut self, day_index: usize, config: &Config ) {
        let mut fasttrack_candidates = self.get_employees_with_shift(day_index, ShiftKind::Morning);

        fasttrack_candidates.sort_by( |a, b| b.fasttrack_score.partial_cmp(&a.fasttrack_score).unwrap() );

        self.set_scores_for_day(day_index, config);

        for candidate in fasttrack_candidates {
            if &self.chart[candidate.index][day_index - 1].kind == &ShiftKind::Morning {
                if !day_index == self.days_of_the_month + CARRIED_DAYS {
                    if &self.chart[candidate.index][day_index + 1].kind == &ShiftKind::Morning {
                        self.chart[candidate.index][day_index] = Shift::from(ShiftKind::FastTrack);

                        self.employee_vector[candidate.index].remove_shift(ShiftKind::Morning);
                        self.employee_vector[candidate.index].add_shift(ShiftKind::FastTrack);

                        return
                    }
                }

            } else if &self.chart[candidate.index][day_index - 1].kind != &ShiftKind::FastTrack {
                self.chart[candidate.index][day_index] = Shift::from(ShiftKind::FastTrack);
    
                self.employee_vector[candidate.index].remove_shift(ShiftKind::Morning);
                self.employee_vector[candidate.index].add_shift(ShiftKind::FastTrack);

                return
            }
        }
    }

    fn get_shift_number_count( &mut self, employee: &Employee, shift_kind: ShiftKind, number: u8 ) -> u8 {
        let mut shift_number_count =  0;

        for shift in &self.chart[employee.index] {
            if shift.kind == shift_kind && shift.number.is_some() {
                if shift.number.unwrap() == number {
                    shift_number_count += 1;
                }
            }
        }

        shift_number_count
    }

    pub fn distribute_numbers( &mut self, day_index: usize ) {
        let numbered_shift_kinds = [ShiftKind::Evening, ShiftKind::Morning];

        for shift_kind in numbered_shift_kinds {
            let mut candidate_employees = self.get_employees_with_shift(day_index, shift_kind);
            
            for number in 1..4 {
                candidate_employees.sort_by(
                    |a, b| self.get_shift_number_count(a, shift_kind, number).partial_cmp(&self.get_shift_number_count(b, shift_kind, number)).unwrap()
                );
        
                self.chart[candidate_employees[0].index][day_index].number = Some(number);
                candidate_employees.remove(0);
            }
        }
    }

    pub fn finish_solving( &mut self, config: &Config ) {
        for day_index in CARRIED_DAYS..self.days_of_the_month+CARRIED_DAYS {
            let (_, _, fasttracks, _) = self.get_days_shifts(day_index);
    
            if fasttracks < ShiftKind::FastTrack.required() {
                self.assign_best_fastrack(day_index, config);
            }
    
            self.distribute_numbers(day_index);
        }      
    }

    fn get_max_employee_name_len( &self ) -> usize {
        let mut max_employee_name_len = 0;
    
        for employee in &self.employee_vector {
            let employee_name_len = employee.name.chars().count();
    
            if employee_name_len > max_employee_name_len {
                max_employee_name_len = employee_name_len;
            }
        }
    
        max_employee_name_len
    }

    fn find_next_assignable( &mut self, config: &Config ) -> ( Option<(usize, usize)>, u8, u8, u8, u8, bool ) {
        for day_index in CARRIED_DAYS..self.days_of_the_month+CARRIED_DAYS {
            let (evenings, mornings, fasttracks, afternoons) = self.get_days_shifts(day_index);
    
            self.set_flags_for_day(day_index);
            self.set_scores_for_day(day_index, config);
    
            let required_afternoons = match self.non_working_indices.contains(&day_index) {
                false => ShiftKind::Afternoon.required(),
                true => 0,
            };
    
            if evenings != ShiftKind::Evening.required() || mornings + fasttracks != ShiftKind::Morning.required() + ShiftKind::FastTrack.required() || afternoons != required_afternoons {
                let mut assignable_employees: Vec<Employee> = Vec::new();

                for employee in &self.employee_vector {
                    if self.chart[employee.index][day_index].is_empty() {
                        if !employee.is_unassignable() {
                            assignable_employees.push( employee.clone() );
                        }
                    } 
                }
    
                if assignable_employees.len() > 0 {
                    assignable_employees.sort_by( |a, b| b.afternoon_score.partial_cmp(&a.afternoon_score).unwrap() );
    
                    for candidate in &assignable_employees {
                        if !candidate.is_resting {
                            self.employee_vector[candidate.index].is_best_afternoon = true; 
                            break
                        }
                    }
    
                    assignable_employees.sort_by( |a, b| b.get_general_score().partial_cmp(&a.get_general_score()).unwrap() );
    
                    let best_candidate_index = assignable_employees[0].index;
                    
                    return ( Some((best_candidate_index, day_index)), evenings, mornings, fasttracks, afternoons, false )
    
                } else {
                    return ( None, 0, 0, 0, 0, true )
                }
    
            }
            
        }
    
        ( None, 0, 0, 0, 0, false )
    }

    pub fn solve_for_calibration( &mut self, solved_charts: &mut Vec<Chart>, config: &Config, progress_bar: &mut bar::ProgressBar ) -> Result<bool, Error> {
        let (empty_cell, evenings, mornings, fasttracks, afternoons, day_unsolvable) = self.find_next_assignable(config);

        if day_unsolvable {
            return Ok(false)
        }
    
        let (employee_index, day_index) = match empty_cell {
            Some(cell) => cell,
            None => {
                progress_bar.job_completed();
                self.false_shifts = 0;
    
                solved_charts.push( self.clone() );
                    
                if !config.be_quiet {
                    progress_bar.display();
                }

                if solved_charts.len() as u64 >= config.calculations {
                    return Err( Error::new(ErrorKind::Other, "Exiting recursion") )
                } else {
                    return Ok(false)
                }
            }
        };
    
        let employee_shift_order = self.employee_vector[employee_index].get_shift_kind_order();

        let required_afternoons = match self.non_working_indices.contains(&day_index) {
            false => ShiftKind::Afternoon.required(),
            true => 0,
        };

        let assigned_shifts_hashmap = HashMap::from([
            ( ShiftKind::Evening, evenings ),
            ( ShiftKind::Morning, mornings + fasttracks ),
            ( ShiftKind::Afternoon, afternoons ),
        ]);
    
        let required_shifts_hashmap = HashMap::from([
            ( ShiftKind::Evening, ShiftKind::Evening.required() ),
            ( ShiftKind::Morning, ShiftKind::Morning.required() + ShiftKind::FastTrack.required() ),
            ( ShiftKind::Afternoon, required_afternoons ),
        ]);
    
        for possible_shift_kind in employee_shift_order {
            let assigned = *assigned_shifts_hashmap.get(&possible_shift_kind).unwrap();
            let required = *required_shifts_hashmap.get(&possible_shift_kind).unwrap();
            
            if assigned < required {
                self.chart[employee_index][day_index] = Shift::from(possible_shift_kind);
                self.employee_vector[employee_index].add_shift(possible_shift_kind);
                
                if self.solve_for_calibration(solved_charts, config, progress_bar)? {
                    return Ok(true)
                }
                    
                self.chart[employee_index][day_index] = Shift::new();
                self.employee_vector[employee_index].remove_shift(possible_shift_kind);
            }
        }

        if self.false_shifts > 500 {
            let remaining_jobs = config.calculations - (progress_bar.completed_jobs % config.calculations);

            for _ in 0..remaining_jobs {
                progress_bar.job_completed();
            }

            return Err( Error::new(ErrorKind::Deadlock, "Took to many tries to solve") )
            
        } else {
            self.false_shifts += 1;
            Ok(false)
        }
    }

    pub fn solve( &mut self, solved_charts: &mut Vec<Chart>, config: &Config, progress_bar: &mut bar::ProgressBar ) -> Result<bool, Error> {
        let (empty_cell, evenings, mornings, fasttracks, afternoons, day_unsolvable) = self.find_next_assignable(config);
    
        if day_unsolvable {
            return Ok(false)
        }
    
        let (employee_index, day_index) = match empty_cell {
            Some(cell) => cell,
            None => {
                progress_bar.job_completed();
    
                solved_charts.push( self.clone() );
                    
                if !config.be_quiet {
                    progress_bar.display();
                }

                if solved_charts.len() as u64 >= config.calculations {
                    return Err( Error::new(ErrorKind::Other, "Exiting recursion") )
                } else {
                    return Ok(false)
                }
            }
        };
    
        let employee_shift_order = self.employee_vector[employee_index].get_shift_kind_order();

        let required_afternoons = match self.non_working_indices.contains(&day_index) {
            false => ShiftKind::Afternoon.required(),
            true => 0,
        };

        let assigned_shifts_hashmap = HashMap::from([
            ( ShiftKind::Evening, evenings ),
            ( ShiftKind::Morning, mornings + fasttracks ),
            ( ShiftKind::Afternoon, afternoons ),
        ]);
    
        let required_shifts_hashmap = HashMap::from([
            ( ShiftKind::Evening, ShiftKind::Evening.required() ),
            ( ShiftKind::Morning, ShiftKind::Morning.required() + ShiftKind::FastTrack.required() ),
            ( ShiftKind::Afternoon, required_afternoons ),
        ]);
    
        for possible_shift_kind in employee_shift_order {
            let assigned = *assigned_shifts_hashmap.get(&possible_shift_kind).unwrap();
            let required = *required_shifts_hashmap.get(&possible_shift_kind).unwrap();
            
            if assigned < required {
                self.chart[employee_index][day_index] = Shift::from(possible_shift_kind);
                self.employee_vector[employee_index].add_shift(possible_shift_kind);
                
                if self.solve(solved_charts, config, progress_bar)? {
                    return Ok(true)
                }
                    
                self.chart[employee_index][day_index] = Shift::new();
                self.employee_vector[employee_index].remove_shift(possible_shift_kind);
            }
        }
    
        Ok(false)
    }

    pub fn get_balance_deviation( &self ) -> f64 {
        let mut hour_balances: Vec<i32> = Vec::new();

        for employee in &self.employee_vector {
            let balance: i32 = employee.worked_hours as i32 - employee.contract_hours as i32;

            hour_balances.push(balance);
        }

        hour_balances.get_stdev()
    }

    pub fn get_shift_balance_deviation( &self ) -> f64 {
        let mut shift_variances: Vec<f64> = Vec::new();

        for employee in &self.employee_vector {
            let mut shift_balances: Vec<u8> = Vec::new();

            shift_balances.push( *employee.shift_counts.get(&ShiftKind::Evening).unwrap() );
            shift_balances.push( *employee.shift_counts.get(&ShiftKind::Morning).unwrap() );
            shift_balances.push( *employee.shift_counts.get(&ShiftKind::FastTrack).unwrap() );
            shift_balances.push( *employee.shift_counts.get(&ShiftKind::Afternoon).unwrap() );

            shift_variances.push( shift_balances.get_variance() );
        }

        let average_variance = shift_variances.get_average();

        average_variance.sqrt()
    }

    pub fn get_shift_deviations( &self ) -> (f64, f64, f64, f64) {
        let mut shift_standard_deviations: HashMap<&ShiftKind, f64> = HashMap::new();

        for shift_kind in &[ShiftKind::Evening, ShiftKind::Morning, ShiftKind::FastTrack, ShiftKind::Afternoon] {
            let mut shift_counts: Vec<u8> = Vec::new();
    
            for employee in &self.employee_vector {
                shift_counts.push( *employee.shift_counts.get(shift_kind).unwrap() );
            }

            *shift_standard_deviations.entry(shift_kind).or_insert(0.0) = shift_counts.get_stdev();
        }
        
        (
            *shift_standard_deviations.values().nth(0).unwrap(),
            *shift_standard_deviations.values().nth(1).unwrap(),
            *shift_standard_deviations.values().nth(2).unwrap(),
            *shift_standard_deviations.values().nth(3).unwrap(),
        )
    }
}
