use crate::cli;
use crate::config::Config;
use crate::exception::{self, CustomErrorKind};

pub struct Flag {
    pub long: &'static str,
    pub short: char,
    pub description: &'static str
}

pub const HELP_FLAG: Flag = Flag { long: "help", short: 'h', description: "Show this help message" };
pub const VERSION_FLAG: Flag = Flag { long: "version", short: 'V', description: "Show program version" };
pub const QUIET_FLAG: Flag = Flag { long: "quiet", short: 'q', description: "Enable quiet mode (returns csv)" };
pub const WORKSHEET_FLAG: Flag = Flag { long: "worksheet", short: 'w', description: "Calculate solutions for this worksheet" };
pub const CALCULATIONS_FLAG: Flag = Flag { long: "calculations", short: 'c', description: "Number of chart solutions to calculate" };
pub const SOLUTIONS_FLAG: Flag = Flag { long: "solutions", short: 's', description: "Number of chart solutions to display" };
pub const API_FLAG: Flag = Flag { long: "api", short: 'A', description: "Run in API mode" };

pub const FLAGS: [Flag; 7] = [
    HELP_FLAG,
    VERSION_FLAG,
    QUIET_FLAG,
    WORKSHEET_FLAG,
    CALCULATIONS_FLAG,
    SOLUTIONS_FLAG,
    API_FLAG,
];

enum ProgramOption {
    Worksheet,
    Calculations,
    Solutions,
    None,
}

pub fn read( arguments: Vec<String>, config: &mut Config ) {
    let mut last_option = ProgramOption::None;

    if arguments.len() > 1 {
        for argument in &arguments[1..] {
            if argument.starts_with("--") {
                match &argument[2..] {
                    x if x == HELP_FLAG.long => cli::show_help(),
                    x if x == VERSION_FLAG.long => cli::show_version(),
                    x if x == QUIET_FLAG.long => config.be_quiet = true,
                    x if x == WORKSHEET_FLAG.long => last_option = ProgramOption::Worksheet,
                    x if x == CALCULATIONS_FLAG.long => last_option = ProgramOption::Calculations,
                    x if x == SOLUTIONS_FLAG.long => last_option = ProgramOption::Solutions,
                    x if x == API_FLAG.long => config.run_api = true,

                    _ => exception::raise(CustomErrorKind::InvalidOption, argument),
                }

            } else if argument.starts_with("-") {
                for char in argument.chars().skip(1) {
                    match char {
                        x if x == HELP_FLAG.short => cli::show_help(),
                        x if x == VERSION_FLAG.short => cli::show_version(),
                        x if x == QUIET_FLAG.short => config.be_quiet = true,
                        x if x == WORKSHEET_FLAG.short => last_option = ProgramOption::Worksheet,
                        x if x == CALCULATIONS_FLAG.short => last_option = ProgramOption::Calculations,
                        x if x == SOLUTIONS_FLAG.short => last_option = ProgramOption::Solutions,
                        x if x == API_FLAG.short => config.run_api = true,

                        _ => exception::raise(CustomErrorKind::InvalidOption, argument),
                    }
                }
                
            } else {
                match last_option {
                    ProgramOption::Worksheet => config.worksheet_name = Some( String::from(argument) ),
                    ProgramOption::Calculations => config.calculations = argument.parse().unwrap(),
                    ProgramOption::Solutions => config.solutions = argument.parse().unwrap(),

                    _ => config.workbook_filepath = argument.clone(),
                }

                last_option = ProgramOption::None;
            }
        }
    }

    if config.solutions > config.calculations {
        config.solutions = config.calculations;
    }
}