use std::env;
use std::io::Error; 
use std::process::Command;

fn get_current_bin_path() -> String {
    return env::current_exe().unwrap().parent().unwrap().display().to_string() + "/"
}

fn main() -> Result<(), Error> {
    let shift_chart_bin_path = get_current_bin_path() + "shift-chart";

    let mut write = Command::new(shift_chart_bin_path)
        .arg("--api")
        .spawn()
        .expect("Failed to run in API mode");

    write.wait()?;

    Ok(())
}